# Vue doc88 test - Liniquer Fractucello
## Front-End test using VueJS, Vuex

![project preview](https://i.imgur.com/9qVprsu.jpg)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```