import Vue from 'vue'
import Vuex from 'vuex'
import OrderStore from './orderStore/index'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    OrderStore
  }
})


