export default {
  namespaced: true,
  state: {
    status: 'no-data',
    switchState: 'comida',
    file: {},
    orders: [],
    ordersFilter: []
  },
  getters: {
    status: state => state.status,
    fileImageUrl: state => state.file.image,
    getSwitchState: state => state.switchState,
    getSwitchOrders: state => {
      if(state.switchState === 'comida') {
        state.ordersFilter = state.orders.filter(order => order.type === 'comida')
      } else {
        state.ordersFilter = state.orders.filter(order => order.type === 'bebida')
      }
    }
  },
  mutations: {
    updateStatus(state, status) {
      state.status = status
    },

    addFile(state, file) {
      state.file = file
    },

    updateSwitchState(state, type) {
      state.switchState = type
    },

    addOrder(state, order) {
      state.orders.unshift(order)
    }
    
  },
  actions: {
    createOrder({ commit }, order) {
      commit('addOrder', order);
    },

    addFileStore({ commit }, file) {
      commit('addFile', file);
    },

    setSwitchState({ commit }, state) {
      commit('updateSwitchState', state);
    },

    setStatus({ commit }, value) {
      commit('updateStatus', value);
    },
  }
}
